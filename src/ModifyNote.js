import React, {useState} from "react";
import { useForm } from "react-hook-form";
import './index.css';
import { useEasybase } from "easybase-react";
import {getIndexesItems} from './tools.js'


export default function ModifyNote({handleModify,indexes, id}) {

	/*Load database*/
	const { Frame, sync } =useEasybase();

	/*Find note of the corresponding patient in the database*/
	var Description = Frame(indexes[id]).note;

	/*Update note when changed*/
	const [note, setNote]=useState(Description);

	/*Callback parent function to modify in the database the note and change the display*/
	const handleSave = () => {
		handleModify(note, id);

  	}

	return(

		<div className="ModifyNote">

		    <textarea name="note" value={note} className="InputModCons" type="string" onChange={e => setNote(e.target.value)}>  </textarea>
			
			<button className="ButtonSmall alignBottomRight" onClick={() => handleSave()}><h3>Enregistrer</h3></button>
		
		</div>	
		
	)

}