import React, {useState} from "react";
import './index.css';
import { useEasybase } from "easybase-react";
import {getIndexesItems} from './tools.js'


export default function TextNote({handleNote, handleDelete, indexes, id}) {

	/*Load database*/
	const { Frame, sync } =useEasybase();
	
	/*find the descirption of the selected date*/
	var Description = Frame(indexes[id]).note;

	/*Callback parent function with the wanted action: modify or delete note*/
	const handleModify = () => {
		handleNote(id);
  	}

	const handleSup = () => {	
		handleDelete(id);
  	}

  	
	return(
		<div >
			<>
				<div className="alignText">
					<p className="textNote">{Description}</p>
				</div>
				
				<div className="alignBottomRight">

					<button className="ButtonSmall " onClick={() => handleModify()}><h3>Modifier</h3></button>
					<button className="ButtonSmall " onClick={() => handleSup()}><h3>Supprimer</h3></button>
				
				</div>
			</>

		</div>
							
		)

}