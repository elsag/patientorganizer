import React, {useState} from "react";
import { EasybaseProvider, useEasybase } from "easybase-react";
import Patient from './Patient.js';
import { useEffect } from 'react';
import './index.css';
import {keepUniqueItems} from './tools.js'
import NewPatientButton from './NewPatient.js';
//import logo from './logo_cuure.png';
import Accueil from './Accueil.js'



export default function Corps(){
	
  /*Load database*/
	const { Frame, sync, configureFrame } =useEasybase();
	
  /*set State for area displayed*/
	const [active, setActive]= useState("Accueil");

	const [name, setName]=useState("Name");

  /*allow to reset state of child when selecting a new patient*/
  const [resetState, setResetState]=useState(0);

	/*Take back the NOTES CONSULTATION table available on easybase*/
	useEffect(() => {
	    configureFrame({ tableName: "NOTES CONSULTATION", limit: 10 });
	    sync();
	  }, []);

  /*Take all patient name only once*/
	var PatientsNames = keepUniqueItems(Frame().map(x => x.name)).sort();


  /*Function call when press on a patient's name*/
  function handleClick(ele){
  		setActive("Patient");
  		setName(ele);
      setResetState(resetState + 1)

  	}

  function handleAccueil(){
    setActive("Accueil");
  }

  return (

    <>
      <div className="headerApp">

          <button className="NewPatientButton" onClick={()=>setActive("New")}>
            <h3>Nouveau Patient</h3>
          </button>

           <h3 className="logo"> </h3>
          
      </div>

      <div className="PatientsList" >
                
          {PatientsNames.map(ele => 
             
          	
            	
            	<button className="namesButton" onClick={()=>{handleClick(ele)}}>
            		<p>{ele}</p>
            	</button>
        
          )}
      </div>
      
        	<div>
        		{active === "Accueil" && <Accueil />}
        		{active === "Patient" && <Patient name={name} key={resetState} handleAccueil={handleAccueil} />}
        		{active === "New" && <NewPatientButton handleClick={handleClick} />}
        	</div>
        

        

      
    </>
  )


}

