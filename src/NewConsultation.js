import React, {useState} from "react";
import './index.css';
import { useEasybase } from "easybase-react";
import {keepUniqueItems} from './tools.js'


export default function NewConsultation({handleNew, handleReturn}) {

	/*var of the new patient's note*/
	var newPatientNote='';

	/*Callback parent function to save note and change display*/
	const handleSave = () => {
		handleNew(newPatientNote);
  	}

	/*Callback parent function and change display*/
	const handleBack = () => {
		handleReturn();
  	}

  	/*Update var newPatientNote when changed*/
  	const updateNote=(value) =>{
      newPatientNote = value;
    }

	return(
		
		<div >
			
	        <p >Notes de consultation </p>
	        
	        <form>
	        	<textarea className="InputNewCons" type="string" onChange={e => updateNote(e.target.value)}> </textarea>
			</form>
	        
	  		<div className="alignBottomRight">
	  			<button className="Button" onClick={() => handleBack()}><p>Annuler</p></button>
	  			<button className="Button" onClick={() => handleSave()}><p>Enregistrer</p></button>
	  		</div>

  		</div>

		)

}