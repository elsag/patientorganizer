import { useEasybase } from "easybase-react";

export function getAllIndexes(arr, val) {
    var indexes = [], i = -1;
    while ((i = arr.indexOf(val, i+1)) != -1){
        indexes.push(i);
    }
    return indexes;
}

export function getIndexesItems(arr, indexes) {
	var items =[], i=0;
	for (let i = 0; i < indexes.length; i++){
		items.push(arr[indexes[i]]);

	}
	return items;
}

export function keepUniqueItems(arr){
	var sort_arr= arr.sort(), unique_arr=[], last= '';

	for (let i = 0; i < sort_arr.length; i++){
		if (sort_arr[i] != last) {
			unique_arr.push(sort_arr[i]); 
			last=sort_arr[i];}
	}
	return unique_arr;

}


export default {getAllIndexes, getIndexesItems, keepUniqueItems}