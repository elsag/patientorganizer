import React, {useState} from "react";
import './index.css';
import { useEasybase } from "easybase-react";
import NewConsultation from './NewConsultation.js'
import DescriptionConsultations from './DescriptionConsultations.js'
import {getAllIndexes,getIndexesItems} from './tools.js'


export default function Patient({name, handleAccueil}){

	/*Load database*/
	const { Frame, sync } =useEasybase();

	/*state of the display area*/
	const [active, setActive]= useState("Description");

	/*allow to reset state of child when selecting a new patient*/
  	/*const [resetStatePatient, setResetStatePatient]=useState(0);*/

	/*Find all existing patient name*/
	const name_list = Frame().map(x => x.name);

	/*Find all indexes in the database with the name of the selected patient*/
	var indexes = getAllIndexes(name_list, name);
	
	/*Callback function to change display*/
	function handleConsultation(){
		setActive("New");
		

    }

    function handleReturn(){
		setActive("Description");
		
    }

    /*Save entry of the new note and change display*/
    function handleNew(notes){
		

		Frame().push({
        name: name,
        note: notes,
        birthday: Frame(indexes[0]).birthday,
        mail: Frame(indexes[0]).mail,
        phone: Frame(indexes[0]).phone,
        date: new Date().toISOString()
      })

	    sync(); 
	  
	    setActive("Description");
      
    }

	return (

		<div className="PatientArea">
		        
	        <div className="headerPatientDate" > 

		        <h3 className="nameUnderline"> {name} </h3> 

		        <div className="InfoPatient">

   					<div className="displayInfo"  ><p className="groupInfo">Téléphone </p> <p className="patientInfo"> {Frame(indexes[0]).phone}</p> </div>
			        <div className="displayInfo" ><p className="groupInfo"> Email </p> <p className="patientInfo"> {Frame(indexes[0]).mail}</p></div>
			        <div className="displayInfo" ><p className="groupInfo"> Age </p> <p className="patientInfo"> {Frame(indexes[0]).birthday}</p> </div>
			     
	        	</div>

	        </div>

	        <div className="content">
	         
				{active === "Description" && <DescriptionConsultations handleConsultation={handleConsultation} name={name} handleAccueil={handleAccueil} />}
				{active === "New" && <NewConsultation handleNew={handleNew} handleReturn={handleReturn} />}

	         </div>
		       
	   	</div>
	)
}
