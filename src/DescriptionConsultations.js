import React, {useState} from "react";
import './index.css';
import { useEasybase } from "easybase-react";
import {getAllIndexes,getIndexesItems} from './tools.js'
import TextNote from './TextNote.js';
import ModifyNote from './ModifyNote.js';


export default function DescriptionConsultations({handleConsultation, name, handleAccueil}) {

	/*Load database*/
	const { Frame, sync } =useEasybase();

	/*Find all existing patient name*/
	const name_list = Frame().map(x => x.name);

	/*Find all indexes in the database with the name of the selected patient*/
	var indexes = getAllIndexes(name_list, name);

	/*Find date and note of the corresponding patient in the database*/
	var DataFrame = getIndexesItems(Frame().map(x => x.date), indexes) ;

	var DescriptionFrame = getIndexesItems(Frame().map(x => x.note), indexes);

	/*set state of the displayed area*/
	var [active, setActive]= useState("Text");

	var [inc, setInc]=useState(0);

	/*Call parent fonction*/
	const handleToNew = () => {
		handleConsultation();
  	}

  	/*function call to change description depending of the mode: Text*/
  	const setState = (i) => {
		setInc(i);
		setActive("Text");
  	}

  	/*Modify note in the database*/
  	function handleModify(Notes, id){
  		const idFrame=indexes[id];

  		Frame()[idFrame].note=Notes;
  		sync();
 		
  		setActive("Text");
  		setInc(id);

  	}

  	/*function call to change description depending of the mode: Modify*/
	function handleNote(id){
		
  		setActive("Modify");
  		setInc(id);
  	}

  	/*Delete entry in the database*/
  	function handleDelete(id){
  		const idFrame=indexes[id];

		delete Frame()[idFrame];
		sync();

		indexes.splice(id);

		DataFrame = getIndexesItems(Frame().map(x => x.date), indexes) ;
		DescriptionFrame = getIndexesItems(Frame().map(x => x.note), indexes);

		console.log(DescriptionFrame)
		console.log("indexes:",indexes)
		
		
  		setActive("Text");
  		setInc(0);
  		
  	}

	return(

		<div className="ConsultationArea" >

			<div className="leftColumn">

				<button className="ButtonNew" onClick={() => handleToNew()}><h3 >Nouvelle note</h3></button>

				{DataFrame.map((x,i) =>  

			      	<button className="PatientDate" onClick={()=>{setState(i)}}>
			      		<p className="DateStyle">{String(x.slice(8,10))}.{String(x.slice(5,7))}.{String(x.slice(0,4))}</p>
			      	</button>

		      	)}

			 </div>

			 <div className="rightColumn">
	  
				<div className="DescriptionArea" >

					{active === "Modify" && <ModifyNote handleModify={handleModify} indexes={indexes} id={inc} />}
					{active === "Text" && <TextNote handleNote={handleNote} handleDelete={handleDelete} indexes={indexes} id={inc} />}

		     	</div>
	  		</div>
					
		</div>
		
	)

}
